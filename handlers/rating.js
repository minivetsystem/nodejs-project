const ratingHandler = require('../handlers/rating.js');
const validation = require('../validation.js');
const rating = {
  method:'POST',
  path:'/v1/rate',
  handler:ratingHandler,
  config: {
    validate: {
      payload: validation.ratevalidation
    },
    auth:'session'
  }
}
module.exports =rating;
