const signUpHandler = require('../handlers/signUp.js');
const validation = require('../validation.js');
const signUp = {
  method:'POST',
  path:'/v1/signup',
  handler:signUpHandler,
  config: {
    validate: {
      payload: validation.signupvalidation
    },
    auth:false
  }
}
module.exports =signUp;
