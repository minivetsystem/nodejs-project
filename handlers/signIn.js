const signInHandler = require('../handlers/signIn.js');
const validation = require('../validation.js');
const signIn = {
  method:'POST',
  path:'/v1/signin',
  handler:signInHandler,
  config: {
    validate: {
      payload: validation.loginvalidation
    },
    auth:false
  }
}
module.exports =signIn;
