const Joi = require('joi');
const signupvalidation = {
  username: Joi.string().min(5).max(25).required(),
  fullname:Joi.string().min(5).max(30).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).max(255).required(),
  location: Joi.string().min(4).max(15).required(),
  bio: Joi.string().min(3).max(255).required(),
  picture: Joi.string().min(10).max(255).required(),
}

const loginvalidation = {
  email: Joi.string().email().required(),
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).min(6).max(20).required()
}

const ratevalidation = {
  meal_id: Joi.number().min(1).required(),
  user_id: Joi.number().min(1).required(),
  rate: Joi.number().min(0).max(10).required()
}
const matchvalidation = {
  location: Joi.string().min(4).max(15).required()
}
const acceptematchvalidation = {
  re: Joi.number().min(-1).max(1).required()
}
module.exports = {
  signupvalidation,
  loginvalidation,
  ratevalidation,
  matchvalidation,
  acceptematchvalidation
}
