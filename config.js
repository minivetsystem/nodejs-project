/*global process*/
// if (process.env.NODE_ENV !== 'production'){
//   require('env2')('./.env');
// }
// const mysql = require('mysql');
// const config = {
//   development: {
//     user: 'root',
//     password: '',
//     database: 'jas_api',
//     port: 3306
//   },
//   test: { 
//     user: 'root',
//     password: '',
//     database: 'jas_api',
//     port: 3306
//   },
//   production: {
//     user: process.env.HEROKU_USER,
//     password: process.env.HEROKU_PASSWORD,
//     database: process.env.HEROKU_DATABASE,
//     host : process.env.HEROKU_HOST,
//     port: process.env.HEROKU_PORT,
//     ssl: process.env.HEROKU_SSL
//   }
// }
// const client = new pg.Client(config[process.env.NODE_ENV]);

// const client = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "",
//    database: 'jas_api'
// });

// client.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
// });
const Keyv = require('keyv');
 
const client = new Keyv('mysql://root:@localhost:3306/jas_api');
client.on('error', function(err) {
  if (err) throw err;
   console.log("Connected!");
});
module.exports = client;
