const users = `CREATE TABLE jas_api.users ( 
            user_id INT NOT NULL AUTO_INCREMENT , 
            username VARCHAR(25) NOT NULL , 
            password VARCHAR(255) NOT NULL , 
            email VARCHAR(100) NOT NULL , 
            fullname VARCHAR(50) NOT NULL , 
            location VARCHAR(50) NOT NULL , 
            bio VARCHAR(255) NOT NULL , 
            picture VARCHAR(255) NOT NULL , 
            PRIMARY KEY (user_id)) ENGINE = InnoDB;`;


const categories = `CREATE TABLE jas_api.categories ( 
                  id INT NULL AUTO_INCREMENT , 
                  name VARCHAR(50) NOT NULL , 
                  PRIMARY KEY (id)) ENGINE = InnoDB;`;

const meals = `CREATE TABLE jas_api.meals ( id INT NOT NULL AUTO_INCREMENT , name VARCHAR(50) NOT NULL , meal_category INT NOT NULL , picture VARCHAR(255) NOT NULL , PRIMARY KEY (id)) ENGINE = InnoDB;`;

const rates =`CREATE TABLE jas_api.rates ( id INT NOT NULL AUTO_INCREMENT , meal_id INT NOT NULL , user_id INT NOT NULL , rates INT NOT NULL , PRIMARY KEY (id)) ENGINE = InnoDB;`;
const matches =`CREATE TABLE jas_api.matches ( id INT NOT NULL AUTO_INCREMENT , first_user_id INT NOT NULL , second_user_id INT NOT NULL , accept_first INT NOT NULL DEFAULT '0' , accept_second INT NOT NULL DEFAULT '0' , PRIMARY KEY (id)) ENGINE = InnoDB;`;

const  categoriesData =` INSERT INTO categories (name)
values
('Vegetarian'),('Vegan'),
('Light'),('Hot/Spicy'),
('Dairy Free'),('Gluten Free');`;

const mealsData= `INSERT INTO meals
 (name,meal_category,picture)
 values
 ('test1','1','url'),
 ('test2','2','url'),
 ('test3','3','url'),
 ('test4','4','url'),
 ('test5','5','url'),
 ('test6','6','url');
 `;
module.exports ={
  users,
  categories,
  meals,
  rates,
  categoriesData,
  mealsData,
  matches
}
