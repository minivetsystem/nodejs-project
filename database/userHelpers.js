const dbutils = require('./dbutils.js');
const hash = require('../backend/hashing.js');

function getuserbyemail(email, cb) {
  const query = `SELECT *
  FROM users
  WHERE email=$1`;
  dbutils.runQuery(query, [email], cb)
}
function getuserbyid(id, cb) {
  const query = `SELECT
  user_id,username,email,
  fullname,location,bio,
  picture
  FROM users
  WHERE user_id=$1`;
  dbutils.runQuery(query, [id], cb)
}
function checkRated(data,cb) {

  const query = `SELECT *
  FROM rates
  WHERE meal_id=$1 AND user_id=$2`;
  dbutils.runQuery(query, [data.meal_id,data.user_id], cb)
}
function getMatch(data,cb) {
  const query = `SELECT *
  FROM matches
  WHERE id=$1 and accept_first=0`;
  dbutils.runQuery(query,data, cb)
}
function getMatch2(data,cb) {
  const query = `SELECT *
  FROM matches
  WHERE id=$1 and accept_second=0`;
  dbutils.runQuery(query,data, cb)
}
function getMatchFirst(data,cb) {

  const query = `SELECT *
  FROM matches
  WHERE id=$1 AND first_user_id=$2`;
  dbutils.runQuery(query, data, cb)
}
function getMatchSecond(data,cb) {

  const query = `SELECT *
  FROM matches
  WHERE id=$1 AND second_user_id=$2`;
  dbutils.runQuery(query,data, cb)
}
function updateMatch(data,cb) {
  var query='';
  if(data[2]=='first_user_id'){
    query = `UPDATE
    matches
    set
    accept_first=$3
    WHERE id=$1 AND first_user_id=$2`;
  }else {
    query = `UPDATE
    matches
    set
    accept_second=$3
    WHERE id=$1 AND second_user_id=$2`;
  }
  dbutils.runQuery(query,[data[0],data[1],data[3]], cb)
}
function rate(data,cb) {

  const query = `INSERT
  INTO rates
  (meal_id,user_id,rate)
  values
  ($1,$2,$3);`;
  dbutils.runQuery(query, [data.meal_id,data.user_id,data.rate], cb)
}

function createuser(data, cb) {
  hash.make(data.password)
    .then(hsh => {
      const query = `INSERT INTO users
    (
      username,
      fullname,
      email,
      password,
      location,
      bio,
      picture
    )
    values
    (
      $1, $2, $3, $4, $5,$6,$7
    )
    `;
      dbutils.runQuery(
        query, [
          data.username,
          data.fullname,
          data.email,
          hsh,
          data.location,
          data.bio,
          data.picture
        ], cb);
    });
}

function  getusersByLocation(data,cb){
  const query = `SELECT
  user_id,username,email,
  fullname,location,bio,
  picture
  from
  users where location=$1 and user_id !=$2 ;
  `;
  dbutils.runQuery(query,data, cb)
}
function getUserLocation (data,cb){
  const query = `SELECT location from
  users where user_id=$1 ;
  `;
  dbutils.runQuery(query,[data], cb)

}
function setMatch(data,cb) {
  const query = `
  INSERT INTO matches
  (first_user_id,second_user_id)
  values
  ($1,$2)
  RETURNING id;
  `;
  dbutils.runQuery(query,data, cb)

}
function getAllMatch(id,cb) {
  const query = `SELECT * from
  matches where second_user_id=$1 and accept_second=0;
  `;
  dbutils.runQuery(query,[id], cb)

}


module.exports = {
  getuserbyemail,
  createuser,
  checkRated,
  rate,
  getusersByLocation,
  getUserLocation,
  setMatch,
  getMatch,
  getMatchFirst,
  getMatchSecond,
  updateMatch,
  getuserbyid,
  getAllMatch,
  getMatch2
}
