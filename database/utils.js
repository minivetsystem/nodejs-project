require('env2')('./.env');
const Bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const hash = (pass, cb) => {
  Bcrypt.genSalt(SALT_WORK_FACTOR, function(error, salt) {
    if (error) {
      // eslint-disable-next-line no-console
      console.log('genSalt :', error)

      throw error
    }
    Bcrypt.hash(pass, salt, cb);
  });
}

module.exports = {
  hash
}
