const client = require('./config.js');
const table = require('../scripts/sql.js');
function runMigrate(cb) {
  const tables = `
  ${table.users}
  ${table.categories}
  ${table.meals}
  ${table.rates}
  ${table.matches}
  ${table.categoriesData}
  ${table.mealsData}
  `

  client.query(tables, cb);
}
function runQuery(query, data, cb) {
  client.query(query, data, cb)
}
module.exports = {
  runMigrate,
  runQuery
}
