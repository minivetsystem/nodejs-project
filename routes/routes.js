module.exports=[
  require ('./signIn.js'),
  require ('./rating.js'),
  require ('./signUp.js'),
  require ('./matching.js'),
  require ('./main.js'),
  require ('./upload.js'),
  require ('./acceptMatch.js'),
  require ('./checkMatch.js'),
];
