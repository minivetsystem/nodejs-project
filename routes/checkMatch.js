const checkMatchHandler = require('../handlers/checkMatch.js');
const checkMatch = {
  method:'POST',
  path:'/v1/check_match',
  handler:checkMatchHandler,
  config: {
    auth:'session'
  }
}
module.exports =checkMatch;
