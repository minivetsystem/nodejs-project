const user = require('../database/userHelpers.js');
module.exports=(req,res)=>{

  const match_id=req.params.match_id;
  const user_id =req.state.sid.user_id;
  const reply = req.payload.re;
  user.getMatch([match_id],(error,result)=>{
    if (error) {
      // eslint-disable-next-line no-console
      console.log('getMatch error :', error)
      return res().code(500)
    }
    if (result.rowCount>0){
      user.getMatchFirst([match_id,user_id],(error,result2)=>{
        if (error) {
          // eslint-disable-next-line no-console
          console.log('getMatchFirst error :', error)
          return res().code(500)
        }
        var column ='first_user_id';
        user.updateMatch([match_id,user_id,column,reply],(error,result2)=>{
          if (error) {
            // eslint-disable-next-line no-console
            console.log('updateMatch error :', error)
            return res().code(500)
          }

          return res({msg:'Match accepted'})
        })
      })
    }else{
      user.getMatch2([match_id],(error,result)=>{
        if (error) {
          // eslint-disable-next-line no-console
          console.log('getMatch error :', error)
          return res().code(500)
        }
        if (result.rowCount>0){
          user.getMatchSecond([match_id,user_id],(error,result2)=>{
            if (error) {
              // eslint-disable-next-line no-console
              console.log('getMatchSecond error :', error)
              return res().code(500)
            }
            var  column ='second_user_id';
            user.updateMatch([match_id,user_id,column,reply],(error,result2)=>{
              if (error) {
                // eslint-disable-next-line no-console
                console.log('updateMatch error :', error)
                return res().code(500)
              }

              return res({msg:'Match accepted'})
            })
          })
        }
        else{
          return res({msg:'Not Allow'})
        }
      })
    }
  })
}
