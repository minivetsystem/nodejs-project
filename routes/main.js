const mainHandler = require('../handlers/main.js');
const main = {
  method:'GET',
  path:'/',
  handler:mainHandler,
  config: {
    auth:false
  }
}
module.exports =main;
