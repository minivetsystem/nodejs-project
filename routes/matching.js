const matchingHandler = require('../handlers/matching.js');
const matching = {
  method:'POST',
  path:'/v1/match',
  handler:matchingHandler,
  config: {
    auth:'session'
  }
}
module.exports =matching;
