const uploadHandler = require('../handlers/upload.js');
const upload = {
  method: 'POST',
  path: '/v1/upload',
  config: {
    handler: uploadHandler.upload,
    auth:'session',
    payload: {
      maxBytes: 209715200,
      output: 'file',
      parse: true
    },
  }
}

module.exports =upload;
